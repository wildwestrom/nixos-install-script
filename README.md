# NixOS Install Script

A hand-rolled bash script to install NixOS.
This is based on the [NixOS Manual](https://nixos.org/manual/nixos/stable/index.html).

A note to newbies:
Never run a script when you don't know what it does.

## Installation

Run this script:
```sh
curl -fsSL https://gitlab.com/wildwestrom/nixos-install-script/-/raw/master/NixInstall.sh | sudo bash
```
