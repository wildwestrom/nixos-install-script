#!/usr/bin/env bash

# This configuration only works in Virtualbox at the moment.
# This could break easily on different systems.
# This is designed to be cloned and run.

TOTALRAM=$(free -g | awk 'NR == 2 {printf $2}')
TOTALSWAP=$((TOTALRAM * 2))

function yes_or_no {
    while true; do
        read -r -p "$* [y/n]: " yn
        case $yn in
            [Yy]*) return 0  ;;  
            [Nn]*) echo "Aborted" ; return  1 ;;
        esac
    done
}


# MBR Partitioning
parted /dev/sda -- mklabel msdos
parted /dev/sda -- mkpart primary 1MiB -${TOTALSWAP}GiB
parted /dev/sda -- mkpart primary linux-swap -${TOTALSWAP}GiB 100%

# Create FS
mkfs.ext4 -L nixos /dev/sda1
mkswap -L swap /dev/sda2
mount /dev/disk/by-label/nixos /mnt
swapon /dev/sda2

# Create config
nixos-generate-config --root /mnt
curl -o /mnt/etc/nixos/configuration.nix https://gitlab.com/wildwestrom/nixos-config/-/raw/master/configuration.nix
# Actual install
nixos-install

# Finish
echo "Remember: Log in as root then add password for user 'main' upon reboot."
yes_or_no "Acknowledge:" && reboot
